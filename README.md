# mo8it.com

My personal website built with [Zola](https://www.getzola.org) and [Tailwind CSS](https://tailwindcss.com).

## License

The blog posts are licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0).

Everything else including templates is under the [AGPLv3 license](LICENSE.txt).

+++
title = "Privacy Policy"
+++

The website `mo8it.com` doesn't collect any personal data of its users.
It also doesn't store any cookies ❌🍪

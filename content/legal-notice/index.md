+++
title = "Legal Notice"
+++

## Hosting

Everything under the domain `mo8it.com` is self-hosted in Germany.

## Address

Mohamad Bitar (Mo)

Lucy-Hillebrand-Str. 4

55128 Mainz

Germany

## Email

mo8it@proton.me

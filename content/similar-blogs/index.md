+++
title = "Similar blogs"
description = "A list of similar blogs"
+++

If you enjoy some of [my blog posts](@/blog/_index.md), you might want to check out the blogs below too:

- [**while-true-do.io**](https://blog.while-true-do.io): Blog about Linux, FOSS, Self-Hosting and more {{ feed(link="https://blog.while-true-do.io/rss") }}
- [**alonely0**](https://alonely0.github.io/blog): Blog about Rust {{ feed(link="https://alonely0.github.io/rss.xml") }}
- [**roomlab wiki**](https://roomlab.eu): Not a traditional blog, but an awesome wiki about Linux and self-hosting.
- [**SpaceDimp**](https://spacedimp.com/blog): Blog about Rust and Linux {{ feed(link="https://spacedimp.com/atom.xml") }}

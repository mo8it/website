+++
title = "Home | mo8it.com"
+++

# Hello, World!

## Blog

You might find something interesting in my technical [**blog**](@/blog/_index.md) if you are interested in the following topics:

- Rust 🦀
- Linux 🐧
- Free Open Source Software 🔓️
- Self-Hosting 📦

## About me

I am Mo (aka mo8it), a PhD student in [Programming Language Research](https://www.pl.informatik.uni-mainz.de) in Mainz, Germany.
I love to pass my passion about computer topics and hope that you find some of my blog posts and projects helpful 😃

You can follow me on the following accounts:

- 🏔️ Codeberg: [codeberg.org/mo8it](https://codeberg.org/mo8it)
- 😾 Github: [github.com/mo8it](https://github.com/mo8it) <small>(Only used to contribute to repos I don't own. My own repos are on Codeberg 🏔️)</small>
- 🐘 Mastodon: <a rel="me" href="https://fosstodon.org/@mo8it">@mo8it@fosstodon.org</a>

## Projects

### Teaching 📚️

- [Rustlings](https://github.com/rust-lang/rustlings): Maintainer of Rustlings, small exercises to get you used to reading and writing Rust code
- [How-2-Julia](https://how-2-julia.mo8it.com) ([repo](https://codeberg.org/mo8it/how-2-julia)): Pluto notebooks with tasks for a 5-day Julia course
- [Dev Tools](https://codeberg.org/mo8it/dev-tools): A 5-day course about development tools (Linux, Git, etc.) (not complete yet)

### Rust Programs 🦀

- [OxiTraffic](https://codeberg.org/mo8it/oxitraffic): Self-hosted, simple and privacy respecting website traffic tracker
- [http-cmd](https://codeberg.org/mo8it/http-cmd): A command line tool to run commands over HTTP (useful hack for static site generators)
- [Ising GUI](https://codeberg.org/mo8it/ising_gui): Live simulation of Ising algorithms
- [Oxiform](https://codeberg.org/mo8it/oxiform): Self-hosted forms for your website (not complete yet)

### Rust Libraries 🦀

- [axum-ctx](https://docs.rs/axum-ctx): Error handling library in Axum inspired by anyhow
- [ising](https://crates.io/crates/ising): Ising simulation and algorithms

## Workspace

[Helix](https://helix-editor.com/) 🧬, [GitUI](https://github.com/extrawurst/gitui) and [Fish](https://fishshell.com/) 🐟 inside [Zellij](https://zellij.dev/) on [Fedora Silverblue](https://fedoraproject.org/silverblue/) 🐧

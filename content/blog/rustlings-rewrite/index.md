+++
title = "Rustlings Rewrite"
description = "Version 6 of Rustlings is a rewrite providing a ton of features and improvements"
date = 2024-07-03
updated = 2024-10-30
[taxonomies]
tags = ["rust"]
+++

[Rustlings](https://github.com/rust-lang/rustlings) is a program with a set of small exercises to get you used to reading and writing Rust code 🦀

I have been working on **version 6** of Rustlings for more than 3 months.
During this time, I rewrote the whole program, extended it, improved the exercises and added idiomatic solutions.
Now, I am happy to finally release it 🎉

In this blog post, I will talk a about my journey of becoming a maintainer and working on the new version 😃

Keep reading if you are interested in creating your own set of Rustlings exercises! 🤩

<!-- more -->

If you are only interested in the new features and changes, check out the [CHANGELOG](https://github.com/rust-lang/rustlings/blob/main/CHANGELOG.md#600-2024-07-03) 🔗

<!-- toc -->

## Becoming a maintainer

I used Rustlings as interactive exercises in my [Rust course at my university](@/blog/teaching-rust/index.md).
I created a fork and later pushed some of the improvements to upstream (the official Rustlings project).
After teaching the course for the second time and understanding how students interact with Rustlings and what their struggles are, I wrote an email to the maintainer [Liv (shadows-withal)](https://github.com/shadows-withal) asking if they need a maintainer.
Liv's reply was very lovely and I am really thankful for their continuous support 🤗

After some days, I was given permissions on Github.
Becoming an open source maintainer _could_ be that easy!
Just start contributing and get some pull requests merged.
Then, ask the maintainers kindly if they need a new maintainer 😃

Don't be disappointed if they don't need any new maintainers.
Introducing new maintainers to a project and communicating with them is extra work that has to be justified.
You can continue contributing without becoming a maintainer.

## What to do as a new maintainer?

After becoming a maintainer, I started working on open issues.
Many issues were either outdated or already resolved.
Cleaning up an issue tracker can be really satisfying! 🧹

To get a better understanding of the (rather small) codebase, I started reading the pieces of the source code that I didn't touch yet.
As a performance addict, I found it very helpful to try to optimize the code while reading it.
This gives me a much deeper understanding.
It is also an opportunity to touch the code without any breaking changes.

After getting comfortable with the codebase, I started introducing my own ideas to improve the project.
It is fine to have a vision as a motivated new maintainer, but make sure to communicate it with the other maintainers and get their approval **before** starting the work.
Your ideas need to align with the vision of the project.

## A new milestone

After getting the approval from Liv, I created a [milestone for version 6](https://github.com/rust-lang/rustlings/milestone/1) on Github.
The milestone is useful to organize everything that has to be done before releasing a new version.

A long-term plan is important, but it doesn't have to be in the form of a milestone.
A shared todo list should be enough ✅

I created a new branch named `v6` where all the development for the new version happened.
It is important to keep the `main` branch functional, especially for people compiling from source.
This also allows you to merge pull requests like fixes to the current version.

## Changes and new features

To avoid duplication, check out the [CHANGELOG of version 6](https://github.com/rust-lang/rustlings/blob/main/CHANGELOG.md#600-2024-07-03) to learn about what is new ✨

The highlights are:

- Easier installation with `cargo install rustlings` 📦️
- UI/UX improvements 🎨
- New interactive list of all exercises 📋️
- Idiomatic solutions provided after you are done with an exercise ✅
- `rust-analyzer` (LSP) support out of the box 🎁
- Clippy lints in every exercise 📎
- Support for third-party exercises 🚀

## Third-Party Exercises

The new support for third-party exercises is something that has to be called out.
You are now able to create your own set of exercises and let the Rustlings program handle them.

For example, [Thelie](https://fosstodon.org/deck/@Thelie@social.linux.pizza) came up with a name idea for third-party exercises: _Toughlings_! 💪🏼
That would be an idea for advanced exercises that users can start with after being done with the official Rustlings exercises.

_Speedlings_ is also an idea for performance oriented exercises 🏎️

What about _Frenchlings_?
A translation of the official Rustlings exercises in French 🇫🇷
This is something that people asked for at least twice.
Now, you can do it without us needing to maintain it.

Maybe _Macrolings_, _Asynclings_ or _Unsafelings_? 🤔

I am very excited about your ideas!
Go ahead and create your set of exercises and we can link to your project in the README of the official Rustlings 📎

## Beta testing

After implementing the main features of version 6, I published a beta version and started a public testing phase.
I am very thankful to all [the feedback](https://github.com/rust-lang/rustlings/issues/1960) that I got.
Most importantly, I need to thank [dotjpg3141](https://github.com/dotjpg3141) and [orhun](https://github.com/orhun) for their extensive feedback 🥰

[dotjpg3141's feedback](https://github.com/rust-lang/rustlings/issues/1960#issuecomment-2080217055) is something that open source projects can only dream of 😍
Contributing to open source is not only about submitting code!

## Try it out!

What are you waiting for? Go ahead and try out the new version 🏃🏼
You can find [instructions in the README](https://github.com/rust-lang/rustlings/tree/main?tab=readme-ov-file#getting-started) for getting started.

If you find any issues in the new version, please [create an issue on Github](https://github.com/rust-lang/rustlings/issues/new).

I wrote all the solutions myself, but I am open to improvements 🤗

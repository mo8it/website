+++
title = "Blog"
description = "A technical blog about Rust, Linux, FOSS and Self-Hosting"
template = "blog.html"
page_template = "post.html"
sort_by = "date"
paginate_by = 10
insert_anchor_links = "heading"
+++

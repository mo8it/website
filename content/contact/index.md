+++
title = "Contact"
+++

Don't hesitate to contact me 🦀:

- 🐘 **Mastodon** (preferred): [@mo8it@fosstodon.org](https://fosstodon.org/@mo8it)
- 📨 **Email**: [mo8it@proton.me](mailto:mo8it@proton.me)

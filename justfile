zola:
	zola serve -f --open --drafts

tailwind:
	npx tailwindcss -w -i input.css -o static/main.css
